package com.hgs.Model.Scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.MoveByAction;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.hgs.Model.PixelBrawl;
import com.hgs.Model.UI.*;

public class BattleScene extends Scene {
    Timer timer;
    UIPlayerbar playerbar;
    UILabel roundlabel;
    UIHealthbar playerhealth;
    UIHealthbar opponenthealth;
    String playername;
    String opponentname;
    UIImage player;
    UIImage opponent;
    UIImage target;
    UIBattleMenu battlemenu;
    UIBattleMenu surrendermenu;
    Array<Animation> weaponanim=new Array<Animation>();
    Array<Animation> spellanim= new Array<Animation>();
    UIImage[]playereffects;
    UIImage[]enemyeffects;
    int effectsize=50;
    int turntime=10;
    int turntimeleft=turntime;
    public BattleScene(final PixelBrawl pb) {
        super(pb);
        this.type= SceneType.BATTLE;
        table.setVisible(false);
        table.top();
        table.setDebug(false);
        playerbar= new UIPlayerbar(table,"playerbar", new Vector2(PixelBrawl.oWIDTH,100));

        playereffects= new UIImage[5];
        playereffects[0]= new UIImage(table,PixelBrawl.skin.getDrawable("paper"+1),"paper"+1,new Vector2(effectsize,effectsize),new int[]{0,25,0,0,0});
        playereffects[1]= new UIImage(table,PixelBrawl.skin.getDrawable("paper"+2),"paper"+1,new Vector2(effectsize,effectsize),new int[]{0,25,0,0,0});
        playereffects[2]= new UIImage(table,PixelBrawl.skin.getDrawable("paper"+3),"paper"+1,new Vector2(effectsize,effectsize),new int[]{0,25,0,0,0});
        playereffects[3]= new UIImage(table,PixelBrawl.skin.getDrawable("paper"+4),"paper"+1,new Vector2(effectsize,effectsize),new int[]{0,25,0,0,0});
        playereffects[4]= new UIImage(table,PixelBrawl.skin.getDrawable("paper"+5),"paper"+1,new Vector2(effectsize,effectsize),new int[]{0,25,0,0,0});

        roundlabel= new UILabel("Round 1\n"+turntime+"s",Align.center,table,"roundlabel",new Vector2(300,PixelBrawl.oHEIGHT/20),0.7f,new int[]{25,0,0,0});

        enemyeffects= new UIImage[5];
        enemyeffects[0]= new UIImage(table,PixelBrawl.skin.getDrawable("paper"+1),"paper"+1,new Vector2(effectsize,effectsize),new int[]{0,0,0,25});
        enemyeffects[1]= new UIImage(table,PixelBrawl.skin.getDrawable("paper"+2),"paper"+1,new Vector2(effectsize,effectsize),new int[]{0,0,0,25});
        enemyeffects[2]= new UIImage(table,PixelBrawl.skin.getDrawable("paper"+3),"paper"+1,new Vector2(effectsize,effectsize),new int[]{0,0,0,25});
        enemyeffects[3]= new UIImage(table,PixelBrawl.skin.getDrawable("paper"+4),"paper"+1,new Vector2(effectsize,effectsize),new int[]{0,0,0,25});
        enemyeffects[4]= new UIImage(table,PixelBrawl.skin.getDrawable("paper"+5),"paper"+1,new Vector2(effectsize,effectsize),new int[]{0,0,0,25});

        for (UIImage effect:playereffects)effect.getImage().setColor(30/255f,50/255f,75/255f,1f);
        for (UIImage effect:enemyeffects)effect.getImage().setColor(30/255f,50/255f,75/255f,1f);

        playerhealth= new UIHealthbar(table,"playerhealth",new Vector2(75,500));
        playerhealth.getPb().setValue(100);
        opponenthealth= new UIHealthbar(table,"opponenthealth",new Vector2(75,500));
        opponenthealth.getPb().setValue(100);
        player= new UIImage(table,PixelBrawl.skin.getDrawable("character"),"player",new Vector2(150,150));
        opponent= new UIImage(table,PixelBrawl.skin.getDrawable("character"),"opponent",new Vector2(150,150));
        battlemenu= new UIBattleMenu(table,"battlemenu",new Vector2(400,100),new int[]{100,0,0,0},false,this);
        surrendermenu= new UIBattleMenu(table,"surrendermenu",new Vector2(400,100),new int[]{100,0,0,0},true,this);
        table.add(playerbar.getContainer()).colspan(15);
        table.row();
        table.add();
        table.add();
        for (int i=0;i<5;i++)table.add(playereffects[i].getImage());
        table.add(roundlabel.getlabelcontainer());
        for (int i=4;i>=0;i--)table.add(enemyeffects[i].getImage());
        table.add();
        table.add();
        table.row();
        table.add(playerhealth.getPb()).left();
        table.add(player.getImage()).expandX();
        table.add().colspan(11);
        table.add(opponent.getImage()).expandX();
        table.add(opponenthealth.getPb()).right();
        table.row();
        table.add(battlemenu.getContainer()).colspan(15);
        for (UIImage effect:playereffects)components.add(effect);
        for (UIImage effect:enemyeffects)components.add(effect);
        components.add(playerbar,roundlabel,playerhealth,opponenthealth);
        components.add(player,opponent,battlemenu);


        battlemenu.getSurrenderbtn().getButton().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {

                    Cell cell;
                    cell= table.getCell(battlemenu.getContainer());
                    table.removeActor(battlemenu.getContainer());
                    table.getCells().removeValue(cell,true);
                    components.removeValue(battlemenu,false);
                    table.invalidate();
                    table.row();
                    table.add(surrendermenu.getContainer()).colspan(15);
                    components.add(surrendermenu);
                    pb.screen.updateview();

            }
        });
        surrendermenu.getCancelbtn().getButton().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                Cell cell;
                cell= table.getCell(surrendermenu.getContainer());
                table.removeActor(surrendermenu.getContainer());
                table.getCells().removeValue(cell,true);
                components.removeValue(surrendermenu,false);
                table.invalidate();
                table.row();
                table.add(battlemenu.getContainer()).colspan(15);
                components.add(battlemenu);
                pb.screen.updateview();
            }
        });
        surrendermenu.getConfirmbtn().getButton().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                Cell cell;
                cell= table.getCell(surrendermenu.getContainer());
                table.removeActor(surrendermenu.getContainer());
                table.getCells().removeValue(cell,true);
                components.removeValue(surrendermenu,false);
                table.invalidate();
                table.row();
                table.add(battlemenu.getContainer()).colspan(15);
                components.add(battlemenu);
                pb.screen.updateview();
                surrender();
            }
        });
    }



   public void reset(){
        battlemenu.clearselection();
        battlemenu.showattackbtns();
        turntimeleft=turntime;
        roundlabel.getLabel().setText("Round 1\n"+turntime+"s");
    }


    public void beginmatch(String player, String opponent){
        pb.client.setIngame(true);
        pb.client.sendCommand("match",new String[]{"timerrequest"});
        this.playername=player;
        this.opponentname=opponent;
        timer= new Timer();
        timer.scheduleTask(new Timer.Task() {
            @Override
            public void run() {
                if (turntimeleft>0){
                    turntimeleft--;
                    roundlabel.getLabel().setText("Round 1\n"+turntimeleft+"s");
                }else {
                    battlemenu.hideattackbtns();
                    cancel();
                }
            }
        },1,1f);
        playerbar.getPlayerlabel().getLabel().setText(playername);
        playerbar.getOpponentlabel().getLabel().setText(opponentname);
        roundlabel.getLabel().setText("Round 1\n"+turntimeleft+"s");
        for (UIImage effect:playereffects)effect.getImage().setColor(30/255f,50/255f,75/255f,1f);
        for (UIImage effect:enemyeffects)effect.getImage().setColor(30/255f,50/255f,75/255f,1f);
        battlemenu.showattackbtns();
        weaponanim.clear();
        spellanim.clear();
    }

    public void chooseattack(String attack){
        pb.client.sendCommand("match",new String[]{"choose",attack});
    }

    public void enableeffect(UIImage[] target,int level){
        target[level-1].getImage().setColor(Color.WHITE);
    }

    public void animateattack(String animtarget,String skill,int level){

        if (skill.equals("paper")){
            if (level==3)
            {
                if (animtarget.equals("player"))enableeffect(playereffects,level);
                else{enableeffect(enemyeffects,level);}
            }
            else
            {
                if (animtarget.equals("player"))enableeffect(enemyeffects,level);
                else enableeffect(playereffects,level);
            }
        }

          if (animtarget.equals("player")){
              target=player;
              if (skill.equals("rock")){
                  weaponanim.add(new Animation(pb.screen.getStage(),"rock"+level,new Vector2(100,100),getopponentpos(),getplayerpos(),-450));
                  pb.screen.updateview();
                  weaponanim.get(weaponanim.size-1).begin();
              }

              if (skill.equals("scissors")){
                  spellanim.add(new Animation(pb.screen.getStage(),"scissors"+level,new Vector2(100,100),new Vector2(getplayerpos().x,getplayerpos().y+player.getSize().y*3),getplayerpos(),0));
                  pb.screen.updateview();
                  spellanim.get(spellanim.size-1).begin();
              }
          }
          if (animtarget.equals("enemy")){
              target=opponent;
              if (skill.equals("rock")){
                  weaponanim.add(new Animation(pb.screen.getStage(),"rock"+level,new Vector2(100,100),getplayerpos(),getopponentpos(),-450));
                  pb.screen.updateview();
                  weaponanim.get(weaponanim.size-1).begin();
              }
              if (skill.equals("scissors")){
                  spellanim.add( new Animation(pb.screen.getStage(),"scissors"+level,new Vector2(100,100),new Vector2(getplayerpos().x,getplayerpos().y+player.getSize().y*3),getplayerpos(),0));
                  pb.screen.updateview();
                  spellanim.get(spellanim.size-1).begin();
              }
          }

    }

    public void surrender (){
        pb.client.sendCommand("match",new String[]{"surrender",playername});
    }

    public UIPlayerbar getPlayerbar() {
        return playerbar;
    }

    public int getTurntimeleft() {
        return turntimeleft;
    }

    public void setTurntimeleft(int turntimeleft) {
        this.turntimeleft = turntimeleft;
        if(turntimeleft<1) battlemenu.hideattackbtns();
        roundlabel.getLabel().setText("Round 1\n"+turntimeleft+"s");
    }
    public Vector2 getplayerpos(){
        float playerposx=player.getImage().localToStageCoordinates(new Vector2(player.getImage().getWidth()/2,player.getImage().getHeight()/2)).x;
        float playerposy=player.getImage().localToStageCoordinates(new Vector2(player.getImage().getWidth()/2,player.getImage().getHeight()/2)).y;
        return new Vector2(playerposx,playerposy);
    }
    public Vector2 getopponentpos(){
        float opponentposx=opponent.getImage().localToStageCoordinates(new Vector2(opponent.getImage().getWidth()/2,opponent.getImage().getHeight()/2)).x;
        float opponentposy=opponent.getImage().localToStageCoordinates(new Vector2(opponent.getImage().getWidth()/2,opponent.getImage().getHeight()/2)).y;
        return new Vector2(opponentposx,opponentposy);
    }

    public void stepanimations(Array<Animation>weaponanim,Array<Animation>spellanim,float deltatime){
        for (Animation anim:weaponanim)anim.step(deltatime);
        for (Animation anim:spellanim)anim.step(deltatime);
    }
    public void scaleanimations(Array<Animation>weaponanim,Array<Animation>spellanim,Vector2 from, Vector2 to,float ratio){
        for (Animation anim:weaponanim)anim.update(from,to,ratio);
        for (Animation anim:spellanim)anim.update(new Vector2(to.x,to.y+opponent.getSize().y*3),to,ratio);
    }

    public UIImage getTarget() {
        return target;
    }

    public UIImage getPlayer() {
        return player;
    }

    public UIImage getOpponent() {
        return opponent;
    }

    public Array<Animation> getWeaponanim() {
        return weaponanim;
    }

    public Array<Animation> getSpellanim() {
        return spellanim;
    }
}
