package com.hgs.Model.Scenes;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.hgs.Model.Game.Upgradable;
import com.hgs.Model.PixelBrawl;
import com.hgs.Model.UI.UIButton;
import com.hgs.Model.UI.UILabel;
import com.hgs.Model.UI.UIShop;

public class ShopScene extends Scene {
    UIButton arrowleft;
    UILabel  shoptitle;
    UIButton arrowright;
    UILabel moneylabel;
    int page=1;
    UIShop rockshop;
    UIShop papershop;
    UIShop scissorshop;
    UIShop armorshop;
    UIShop healthshop;
    UIShop magicresistshop;

    Upgradable rock;
    Upgradable paper;
    Upgradable scissors;
    Upgradable armor;
    Upgradable health;
    Upgradable magicresist;
    Array<UIShop>shops;
    public ShopScene(PixelBrawl pb) {
        super(pb);
        this.type=SceneType.SHOP;
        table.setVisible(false);
        table.center();
        arrowleft=  new UIButton("",table,"arrowleft",new Vector2(100,100),new int[]{0,0,50,0});
        arrowleft.setbackground("arrowleft");
        shoptitle=  new UILabel("Skills", Align.center,table,"shoptitlelabel",new Vector2(400,100),2f,new int[]{0,0,50,0});
        arrowright= new UIButton("",table,"arrowright",new Vector2(100,100),new int[]{0,0,50,0});
        arrowright.setbackground("arrowright");
        moneylabel = new UILabel("Money: 5$", Align.center,table,"moneylable",new Vector2(400,100),1f,new int[]{25,0,25,0});
        rock = new Upgradable(Upgradable.Type.Rock);
        paper = new Upgradable(Upgradable.Type.Paper);
        scissors = new Upgradable(Upgradable.Type.Scissors);
        armor= new Upgradable(Upgradable.Type.Armor);
        health= new Upgradable(Upgradable.Type.Health);
        magicresist= new Upgradable(Upgradable.Type.MagicResist);
        rockshop = new UIShop(this,rock,table,"rockshop",new Vector2(350,700));
        papershop = new UIShop(this,paper,table,"papershop",new Vector2(350,700),new int[]{0,100,0,100});
        scissorshop = new UIShop(this,scissors,table,"scissorshop",new Vector2(350,700));
        armorshop = new UIShop(this,armor,table,"armorshop",new Vector2(350,700));
        healthshop= new UIShop(this,health,table,"healthshop",new Vector2(350,700),new int[]{0,100,0,100});
        magicresistshop= new UIShop(this,magicresist,table,"magicresistshop",new Vector2(350,700));
        shops= new Array<UIShop>();
        shops.add(rockshop,papershop,scissorshop);
        shops.add(armorshop,healthshop,magicresistshop);
        table.add(arrowleft.getButton());
        table.add(shoptitle.getlabelcontainer());
        table.add(arrowright.getButton());
        table.row();
        table.add(moneylabel.getlabelcontainer()).colspan(3);
        table.row();
        table.add(rockshop.getContainer(), papershop.getContainer(), scissorshop.getContainer());
        components.add(arrowleft,shoptitle,arrowright,moneylabel);
        components.add(rockshop,papershop,scissorshop);

        arrowleft.getButton().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                swappage();
            }
        });

        arrowright.getButton().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                swappage();
            }
        });

    }
    public void swappage(){
        if (page==1){
            shoptitle.getLabel().setText("Stats");
            Cell cell;

            cell= table.getCell(rockshop.getContainer());
            table.removeActor(rockshop.getContainer());
            table.getCells().removeValue(cell,true);
            components.removeValue(rockshop,false);

            cell= table.getCell(papershop.getContainer());
            table.removeActor(papershop.getContainer());
            table.getCells().removeValue(cell,true);
            components.removeValue(papershop,false);

            cell= table.getCell(scissorshop.getContainer());
            table.removeActor(scissorshop.getContainer());
            table.getCells().removeValue(cell,true);
            components.removeValue(scissorshop,false);

            table.invalidate();
            table.row();
            table.add(armorshop.getContainer());
            table.add(healthshop.getContainer());
            table.add(magicresistshop.getContainer());
            components.add(armorshop,healthshop,magicresistshop);
            page=2;
        }
        else {
            shoptitle.getLabel().setText("Skills");
            Cell cell;

            cell= table.getCell(armorshop.getContainer());
            table.removeActor(armorshop.getContainer());
            table.getCells().removeValue(cell,true);
            components.removeValue(armorshop,false);

            cell= table.getCell(healthshop.getContainer());
            table.removeActor(healthshop.getContainer());
            table.getCells().removeValue(cell,true);
            components.removeValue(healthshop,false);

            cell= table.getCell(magicresistshop.getContainer());
            table.removeActor(magicresistshop.getContainer());
            table.getCells().removeValue(cell,true);
            components.removeValue(magicresistshop,false);

            table.invalidate();
            table.row();
            table.add(rockshop.getContainer());
            table.add(papershop.getContainer());
            table.add(scissorshop.getContainer());
            components.add(rockshop,papershop,scissorshop);
            page=1;
        }
        pb.screen.updateview();
    };
    public void upgrade(int toupgrade){
        pb.client.sendCommand("upgrade",new String[]{String.valueOf(toupgrade)});
    }

    public void setMoney(float Money) {
        moneylabel.getLabel().setText(String.valueOf(Money)+"$");
    }
    public void setlevels(Array<Integer> levels){
        for (int i=0;i<=5;i++){
            shops.get(i).getUpgradable().setlevel(levels.get(i));
            shops.get(i).update();
        }
    }

    public Array<UIShop> getShops() {
        return shops;
    }
}
