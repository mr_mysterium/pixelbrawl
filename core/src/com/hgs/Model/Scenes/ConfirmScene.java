package com.hgs.Model.Scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.hgs.Model.PixelBrawl;
import com.hgs.Model.UI.UIButton;
import com.hgs.Model.UI.UILabel;
import com.hgs.Model.UI.UITextfield;

public class ConfirmScene extends Scene {
    public enum Mode {
       userchange,passwordchange,userdelete

    }

    Mode mode;

    UILabel usernamelabel;
    UITextfield usernamefield;

    UILabel passwordlabel;
    UITextfield passwordfield;

    UILabel newpasswordlabel;
    UITextfield newpasswordfield;

    UILabel newpasswordrepeatlabel;
    UITextfield newpasswordrepeatfield;

    UILabel statuslabel;
    UIButton cancelbtn;
    UIButton confirmbtn;

    public ConfirmScene(final PixelBrawl pb) {
        super(pb);
        this.type=SceneType.CONFIRM;
        table.setVisible(false);
        table.center();
        usernamelabel = new UILabel("New Username:", Align.left,table,"usernamelabel",new Vector2(600,50),0.4f,new int[]{200,0,0,0});
        usernamefield = new UITextfield(table,"usernamefield",new Vector2(600,80),false,new int[]{0,0,0,0});
        usernamefield.enableFilter();

        passwordlabel = new UILabel("Password:", Align.left,table,"passwordlabel",new Vector2(600,50),0.4f,new int[]{25,0,0,0});
        passwordfield = new UITextfield(table,"passwordfield",new Vector2(600,80),true,new int[]{0,0,0,0});
        passwordfield.enableFilter();

        newpasswordlabel = new UILabel("New Password:", Align.left,table,"newpasswordlabel",new Vector2(600,50),0.4f,new int[]{25,0,0,0});
        newpasswordfield = new UITextfield(table,"newpasswordfield",new Vector2(600,80),true,new int[]{0,0,0,0});
        newpasswordfield.enableFilter();

        newpasswordrepeatlabel = new UILabel("Confirm New Password:", Align.left,table,"confirmpasswordlabel",new Vector2(600,50),0.4f,new int[]{25,0,0,0});
        newpasswordrepeatfield = new UITextfield(table,"confirmpasswordfield",new Vector2(600,80),true,new int[]{0,0,0,0});
        newpasswordrepeatfield.enableFilter();

        statuslabel = new UILabel("", Align.center,table,"statuslabel",new Vector2(400,50),0.7f,new int[]{20,0,0,0});

        cancelbtn =  new  UIButton("Cancel",table,"cancelbtn",new Vector2(200,100),new int[]{20,0,0,0});
        confirmbtn = new  UIButton("Confirm",table,"confirmbtn",new Vector2(200,100),new int[]{20,0,0,0});

        cancelbtn.getButton().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                pb.screen.hidescene(SceneType.CONFIRM);
                pb.accountscene.show();
            }
        });

        confirmbtn.getButton().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                confirm();
            }
        });

    }

    public void request(final Mode mode){
        reset();
        table.clear();
        components.clear();
        switch (mode) {
            case userchange:
                table.add(usernamelabel.getlabelcontainer()).colspan(2).left();
                table.row();
                table.add(usernamefield.getTextfield()).colspan(2);
                table.row();
                table.add(passwordlabel.getlabelcontainer()).colspan(2).left();
                table.row();
                table.add(passwordfield.getTextfield()).colspan(2);
                table.row();
                components.add(usernamelabel,usernamefield,passwordlabel,passwordfield);
                setMode(Mode.userchange);
                break;

            case passwordchange:
                table.add(passwordlabel.getlabelcontainer()).colspan(2).left();
                table.row();
                table.add(passwordfield.getTextfield()).colspan(2);
                table.row();
                table.add(newpasswordlabel.getlabelcontainer()).colspan(2).left();
                table.row();
                table.add(newpasswordfield.getTextfield()).colspan(2);
                table.row();
                table.add(newpasswordrepeatlabel.getlabelcontainer()).colspan(2).left();
                table.row();
                table.add(newpasswordrepeatfield.getTextfield()).colspan(2);
                table.row();
                components.add(passwordlabel,passwordfield,newpasswordlabel,newpasswordfield);
                components.add(newpasswordrepeatlabel,newpasswordrepeatfield);
                setMode(Mode.passwordchange);
                break;

            case userdelete:
                table.add(passwordlabel.getlabelcontainer()).colspan(2).left();
                table.row();
                table.add(passwordfield.getTextfield()).colspan(2);
                table.row();
                components.add(passwordlabel,passwordfield);
                setMode(Mode.userdelete);
                break;

            default:
                break;
        }


        table.add(statuslabel.getlabelcontainer()).colspan(2);
        table.row();
        table.add(cancelbtn.getButton());
        table.add(confirmbtn.getButton());
        components.add(statuslabel,cancelbtn,confirmbtn);
        pb.screen.updateview();

    }

    public void setMode(Mode mode) {
        this.mode = mode;
    }

    private void confirm() {
        switch (mode) {
            case userchange:
                statuslabel.getLabel().setColor(Color.RED);
                if (usernamefield.getTextfield().getText().length()==0){

                    statuslabel.getLabel().setText("Username cant be empty");
                    return;
                }

                if (usernamefield.getTextfield().getText().length()<4){
                    statuslabel.getLabel().setText("Username must be at least 4 characters");
                    return;
                }

                if (passwordfield.getTextfield().getText().length()==0){
                    statuslabel.getLabel().setText("Password cant be empty");
                    return;
                }
                statuslabel.getLabel().setColor(Color.WHITE);
                statuslabel.getLabel().setText("Sending...");
                pb.client.sendCommand("rename",new String[]{usernamefield.getTextfield().getText(),passwordfield.getTextfield().getText()});
                break;

            case passwordchange:
                statuslabel.getLabel().setColor(Color.RED);

                if (passwordfield.getTextfield().getText().length()==0){
                    statuslabel.getLabel().setText("Password cant be empty");
                    return;
                }
                if (newpasswordfield.getTextfield().getText().length()==0){
                    statuslabel.getLabel().setText("New Password cant be empty");
                    return;
                }
                if (newpasswordfield.getTextfield().getText().length()<6){
                    statuslabel.getLabel().setText("New Password must be at least 6 characters");
                    return;
                }
                if (!newpasswordfield.getTextfield().getText().equals(newpasswordrepeatfield.getTextfield().getText())) {
                    statuslabel.getLabel().setText("Passwords dont match");
                    return;

                }
                statuslabel.getLabel().setColor(Color.WHITE);
                statuslabel.getLabel().setText("Sending...");
                pb.client.sendCommand("changepass",new String[]{newpasswordfield.getTextfield().getText(),passwordfield.getTextfield().getText()});
                break;

            case userdelete:
                statuslabel.getLabel().setColor(Color.RED);

                if (passwordfield.getTextfield().getText().length()==0){
                    statuslabel.getLabel().setText("Password cant be empty");
                    return;
                }

                statuslabel.getLabel().setColor(Color.WHITE);
                statuslabel.getLabel().setText("Sending...");
                pb.client.sendCommand("delete",new String[]{passwordfield.getTextfield().getText()});
                break;

            default:
                break;
        }
    }

    public void reset(){
        usernamefield.getTextfield().setText("");
        passwordfield.getTextfield().setText("");
        newpasswordfield.getTextfield().setText("");
        newpasswordrepeatfield.getTextfield().setText("");
        statuslabel.getLabel().setText("");
    }

    public UILabel getStatuslabel() {
        return statuslabel;
    }
}
