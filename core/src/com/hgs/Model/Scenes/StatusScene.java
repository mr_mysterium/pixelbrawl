package com.hgs.Model.Scenes;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Align;
import com.hgs.Model.PixelBrawl;
import com.hgs.Model.UI.UILabel;

public class StatusScene extends Scene {
    UILabel infolabel;
    UILabel statuslabel;
    public StatusScene(PixelBrawl pb) {
        super(pb);
        this.type=SceneType.STATUS;
        table.setVisible(false);
        table.center();
        table.right();
        infolabel=new UILabel("Stats",Align.center,table,"infolabel",new Vector2(400,100),1.2f);
        statuslabel=new UILabel("\n\n\n\n\n\n\n-Username-\n\n\n-Money-\n\n\n-Wins-\n\n\n-Losses-", Align.center,table,"infopane",new Vector2(400,800),0.7f);
        statuslabel.getLabel().setWrap(true);
        statuslabel.getlabelcontainer().setBackground(PixelBrawl.skin.getDrawable("infopane"));
        table.add(infolabel.getlabelcontainer());
        table.row();
        table.add(statuslabel.getlabelcontainer()).width(200);
        table.row();
        components.add(infolabel,statuslabel);
    }
    public void setStatus(String username,float money, int wins , int loses){
        statuslabel.getLabel().setText("-Username-\n"+username+"\n\n\n-Money-\n"+money+"$"+"\n\n\n-Wins-\n"+wins+"\n\n\n-Losses-\n"+loses);
    }
}
