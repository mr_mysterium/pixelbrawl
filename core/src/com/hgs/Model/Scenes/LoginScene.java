package com.hgs.Model.Scenes;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.hgs.Model.PixelBrawl;
import com.hgs.Model.UI.UILabel;
import com.hgs.Model.UI.UIButton;
import com.hgs.Model.UI.UITextfield;

public class LoginScene extends Scene {
    UITextfield usernamefield;
    UILabel usernamelabel;
    UITextfield passwordfield;
    UILabel passwordlabel;
    UIButton loginbutton;
    UIButton backbutton;
    UILabel statuslabel;
    public LoginScene(final PixelBrawl pb) {
        super(pb);
        this.type=SceneType.LOGIN;
        table.setVisible(false);
        table.top();
        usernamefield = new UITextfield(table,"usernamefield",new Vector2(800,80),false);
        usernamefield.enableFilter();
        usernamelabel = new UILabel("Username:", Align.left,table,"usernamelabel",new Vector2(800,50),0.5f,new int[]{250,0,0,0});
        passwordfield = new UITextfield(table,"passwordfield",new Vector2(800,80),true);
        passwordfield.enableFilter();
        passwordlabel = new UILabel("Password:", Align.left,table,"passwordlabel",new Vector2(800,50),0.5f,new int[]{50,0,0,0});
        statuslabel = new UILabel("",Align.center,table,"statuslabel",new Vector2(600, 50),0.5f,new int[]{25,0,25,0});
        loginbutton = new UIButton("Login",table,"loginbutton",new Vector2(400,200),new int[]{0,0,0,0});
        backbutton = new UIButton("Back",table,"backbutton",new Vector2(400,200),new int[]{0,0,0,0});
        table.add(usernamelabel.getlabelcontainer()).left().colspan(2);
        table.row();
        table.add(usernamefield.getTextfield()).colspan(2);
        table.row();
        table.add(passwordlabel.getlabelcontainer()).left().colspan(2);
        table.row();
        table.add(passwordfield.getTextfield()).colspan(2);
        table.row();
        table.add(statuslabel.getlabelcontainer()).colspan(2);
        table.row();
        table.add(backbutton.getButton());
        table.add(loginbutton.getButton());
        components.add(usernamefield,passwordfield,usernamelabel,passwordlabel);
        components.add(statuslabel,loginbutton,backbutton);

        backbutton.getButton().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                pb.loginscene.getTable().setVisible(false);
                pb.titlescene.getRegisterbtn().getButton().setVisible(true);
                pb.titlescene.getLoginbtn().getButton().setVisible(true);
                statuslabel.getLabel().setText("");
                pb.registerscene.getStatuslabel().getLabel().setText("");
                usernamefield.getTextfield().setText("");
                passwordfield.getTextfield().setText("");
            }
        });

        loginbutton.getButton().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                statuslabel.getLabel().setColor(Color.RED);
                if (usernamefield.getTextfield().getText().length()==0){
                    statuslabel.getLabel().setText("Username cant be empty");
                    return;
                }
                if (passwordfield.getTextfield().getText().length()==0){
                    statuslabel.getLabel().setText("Password cant be empty");
                    return;
                }
                pb.client.sendCommand("login",new String[]{usernamefield.getTextfield().getText(),passwordfield.getTextfield().getText()});
            }
        });
    }

    public UITextfield getUsernamefield() {
        return usernamefield;
    }

    public UILabel getUsernamelabel() {
        return usernamelabel;
    }

    public UITextfield getPasswordfield() {
        return passwordfield;
    }

    public UILabel getPasswordlabel() {
        return passwordlabel;
    }

    public UIButton getLoginbutton() {
        return loginbutton;
    }

    public UIButton getBackbutton() {
        return backbutton;
    }

    public UILabel getStatuslabel() {
        return statuslabel;
    }

    public void reset(){
        usernamefield.getTextfield().setText("");
        passwordfield.getTextfield().setText("");
        statuslabel.getLabel().setText("");
    }
}
