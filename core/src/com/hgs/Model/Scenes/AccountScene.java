package com.hgs.Model.Scenes;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.hgs.Model.PixelBrawl;
import com.hgs.Model.UI.UIButton;
import com.hgs.Model.UI.UILabel;

public class AccountScene extends Scene {
    UILabel accountlLabel;
    UIButton logoutbtn;
    UIButton changeusernamebtn;
    UIButton changepasswordbtn;
    UIButton deleteuserbtn;
    public AccountScene(final PixelBrawl pb) {
        super(pb);
        this.type=SceneType.ACCOUNT;
        table.setVisible(false);
        table.center();
        accountlLabel= new UILabel("Account", Align.center,table,"accountlabel",new Vector2(600,100),2f,new int[]{0,0,100,0});
        logoutbtn= new UIButton("Logout",table,"logoutbtn",new Vector2(450,150),new int[]{0,0,25,0});
        changeusernamebtn= new UIButton("Change Username",table,"logoutbtn",new Vector2(450,150),0.7f,new int[]{0,0,25,0});
        changepasswordbtn= new UIButton("Change Password",table,"logoutbtn",new Vector2(450,150),0.7f,new int[]{0,0,25,0});
        deleteuserbtn= new UIButton("Delete Account",table,"logoutbtn",new Vector2(450,150),0.7f,new int[]{0,0,0,0});
        deleteuserbtn.getButton().getLabel().setColor(Color.RED);
        table.add(accountlLabel.getlabelcontainer());
        table.row();
        table.add(logoutbtn.getButton());
        table.row();
        table.add(changeusernamebtn.getButton());
        table.row();
        table.add(changepasswordbtn.getButton());
        table.row();
        table.add(deleteuserbtn.getButton());
        components.add(accountlLabel,logoutbtn,changeusernamebtn,changepasswordbtn);
        components.add(deleteuserbtn);

        logoutbtn.getButton().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                pb.client.sendCommand("logout",new String[]{});
            }
        });

        changeusernamebtn.getButton().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                hide();
                pb.screen.showscene(SceneType.CONFIRM);
                pb.confirmscene.request(ConfirmScene.Mode.userchange);
            }
        });

        changepasswordbtn.getButton().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                hide();
                pb.screen.showscene(SceneType.CONFIRM);
                pb.confirmscene.request(ConfirmScene.Mode.passwordchange);
            }
        });

        deleteuserbtn.getButton().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                hide();
                pb.screen.showscene(SceneType.CONFIRM);
                pb.confirmscene.request(ConfirmScene.Mode.userdelete);
            }
        });
    }
    public void hide(){
        for (Cell<Actor>cell: table.getCells())cell.getActor().setVisible(false);
        accountlLabel.getlabelcontainer().setVisible(true);
    }
    public void show(){
        for (Cell<Actor>cell: table.getCells())cell.getActor().setVisible(true);
    }
}
