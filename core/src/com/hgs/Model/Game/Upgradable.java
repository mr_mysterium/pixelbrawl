package com.hgs.Model.Game;

import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Array;
import com.hgs.Model.PixelBrawl;

public class Upgradable {

    public enum Type {
        Rock, Paper, Scissors, Armor, Health, MagicResist
    }
    Type type;
    Drawable typeicon;
    Array<Drawable> skillicon=new Array<Drawable>();
    float[] stat={0,0,0,0,0};
    String effect="";
    int level=0;
    int maxlevel=0;
    float[] cost={0,0,0,0,0};
    float[]chances={0,0,0,0,0};

    public Upgradable(Type type){
        this.type= type;
        switch (type) {
            case Rock:
                typeicon=PixelBrawl.skin.getDrawable("rockicon");
                for (int i=1;i<=5;i++)skillicon.add(PixelBrawl.skin.getDrawable("rock"+i+"icon"));
                stat= new float[]{50,75,100,150,200};
                cost= new float[]{25,50,75,100,150};
                chances= new float[]{100,50,25,10,5};
                maxlevel=5;
                break;
            case Paper:
                typeicon=PixelBrawl.skin.getDrawable("papericon");
                for (int i=1;i<=5;i++)skillicon.add(PixelBrawl.skin.getDrawable("paper"+i+"icon"));
                stat= new float[]{50,100,50,100,15};
                cost= new float[]{25,50,75,100,150};
                chances= new float[]{100,50,25,10,5};
                maxlevel=5;
                break;
            case Scissors:
                typeicon=PixelBrawl.skin.getDrawable("scissorsicon");
                for (int i=1;i<=5;i++)skillicon.add(PixelBrawl.skin.getDrawable("scissors"+i+"icon"));
                stat= new float[]{50,75,100,150,200};
                cost= new float[]{25,50,75,100,150};
                chances= new float[]{100,50,25,10,5};
                maxlevel=5;
                break;
            case Armor:
                typeicon=PixelBrawl.skin.getDrawable("armoricon");
                stat= new float[]{5,10,15,20,25,30,35,40};
                cost= new float[]{25,50,75,100,125,150,175,200};
                chances= new float[]{100,80,60,50,40,30,20,10};
                maxlevel=8;
                break;
            case Health:
                typeicon=PixelBrawl.skin.getDrawable("hpicon");
                stat= new float[]{100,125,150,175,200,250,300,400};
                cost= new float[]{25,50,75,100,125,150,175,200};
                chances= new float[]{100,80,60,50,40,30,20,10};
                maxlevel=8;
                break;
            case MagicResist:
                typeicon=PixelBrawl.skin.getDrawable("magicresisticon");
                stat= new float[]{5,10,15,20,25,30,35,40};
                cost= new float[]{25,50,75,100,125,150,175,200};
                chances= new float[]{100,80,60,50,40,30,20,10};
                maxlevel=8;
                break;
            default:
                break;

         }
    }

    public void setlevel(int level){
        this.level=level;
        switch (type) {
            case Rock:
                effect="Melee Damage "+stat[level-1]+"HP";
                break;
            case Paper:
                switch (level){
                    case 1:
                        effect="Increase Armor";
                        break;
                    case 2:
                        effect="Dodge Attack";
                        break;
                    case 3:
                        effect="Sleep Melody";
                        break;
                    case 4:
                        effect="Increase Damage";
                        break;
                    case 5:
                        effect="Regenerate Health";
                        break;
                    default:
                        break;
                }
                break;
            case Scissors:
                effect="Magic Damage "+stat[level-1]+"HP";
                break;
            case Armor:
                effect="Melee Damage -"+stat[level-1]+"%";
                break;
            case Health:
                effect="Health Points "+stat[level-1]+"HP";
                break;
            case MagicResist:
                effect="Magic Damage -"+stat[level-1]+"%";
                break;
            default:
                break;

        }
    }

    public Drawable getTypeicon() {
        return typeicon;
    }

    public Drawable getSkillicon() {
        return skillicon.get(level-1);
    }

    public Type getType() {
        return type;
    }

    public String getEffect() {
        return effect;
    }

    public float getDamage() {
        return stat[level-1];
    }

    public int getLevel() {
        return level;
    }

    public int getMaxlevel() {
        return maxlevel;
    }

    public float getCost() {
        return cost[level-1];
    }

    public float getChance() {
        return chances[level-1];
    }
}
