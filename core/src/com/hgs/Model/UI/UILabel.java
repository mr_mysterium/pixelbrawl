package com.hgs.Model.UI;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.hgs.Model.PixelBrawl;


public class UILabel extends UIComponent{
    int align;
    Label label;
    Container<Label> labelcontainer;
    String text;
    float scale;
    public  UILabel(String text,int align,Table table, String name, Vector2 size,float scale){
        super(table,name,size);
        this.align=align;
        this.text=text;
        this.scale=scale;
        setStyle();
    }
    public UILabel(String text,int align, Table table,String name,Vector2 size,float scale,int[] padding){
        super(table,name,size,padding);
        this.align=align;
        this.text=text;
        this.scale=scale;
        setStyle();
    }

    @Override
    void setStyle() {
        label = new Label(text, PixelBrawl.skin,"font", Color.WHITE);
        labelcontainer= new Container<Label>();
        labelcontainer.setActor(label);
        labelcontainer.fill();
        label.setAlignment(align);
        actor=labelcontainer;

    }

    @Override
    public void scale(float ratio) {
        table.getCell(actor).size((int) Math.ceil(size.x*ratio),(int)Math.ceil(size.y*ratio));
        table.getCell(actor).padTop(padding[0]*ratio);
        table.getCell(actor).padRight(padding[1]*ratio);
        table.getCell(actor).padBottom(padding[2]*ratio);
        table.getCell(actor).padLeft(padding[3]*ratio);
        label.setFontScale(size.x/200*ratio*scale);

    }

    public Container<Label> getlabelcontainer() {
        return labelcontainer;
    }

    public Label getLabel() {
        return label;
    }
}
