package com.hgs.Model.UI;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Array;
import com.hgs.Model.PixelBrawl;
import com.hgs.Model.Scenes.BattleScene;

public class UIBattleMenu extends UIComponent {
    BattleScene bs;
    Container<Table> container;
    Table t;
    UIButton rockbtn;
    UIButton paperbtn;
    UIButton scissorbtn;
    UIButton surrenderbtn;
    UIButton confirmbtn;
    UIButton cancelbtn;
    UIButton selected;
    boolean surrenderstate;
    Array<UIComponent> components;
    public UIBattleMenu(Table table, String name, Vector2 size,boolean surrenderstate,BattleScene bs) {
        super(table, name, size);
        this.surrenderstate=surrenderstate;
        this.bs=bs;
        setStyle();
    }

    public UIBattleMenu(Table table, String name, Vector2 size, int[] padding,boolean surrenderstate,BattleScene bs) {
        super(table, name, size, padding);
        this.surrenderstate=surrenderstate;
        this.bs=bs;
        setStyle();
    }

    @Override
    void setStyle() {
        t=new Table();
        t.top();
        t.setDebug(false);

        components= new Array<UIComponent>();
        rockbtn= new UIButton("",t,"rock",new Vector2(100,100));
        rockbtn.setbackground("rockicon");

        paperbtn= new UIButton("",t,"paper",new Vector2(100,100),new int[]{0,25,0,25});
        paperbtn.setbackground("papericon");

        scissorbtn= new UIButton("",t,"scissors",new Vector2(100,100));
        scissorbtn.setbackground("scissorsicon");

        surrenderbtn= new UIButton("surrender",t,"surrenderbtn",new Vector2(350,100),new int[]{50,0,0,0});
        cancelbtn= new UIButton("cancel",t,"cancelbtn",new Vector2(350,100),new int[]{50,0,0,0});
        confirmbtn= new UIButton("confirm",t,"confirmbtn",new Vector2(350,100),new int[]{0,0,0,0});

        rockbtn.getButton().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
              clearselection();
              rockbtn.setbackground("rockiconselected");
              bs.chooseattack("rock");
              setselectedButton(rockbtn);
            }
        });

        paperbtn.getButton().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                clearselection();
                paperbtn.setbackground("papericonselected");
                bs.chooseattack("paper");
                setselectedButton(paperbtn);
            }
        });

        scissorbtn.getButton().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                clearselection();
                scissorbtn.setbackground("scissorsiconselected");
                bs.chooseattack("scissors");
                setselectedButton(scissorbtn);
            }
        });

        if (surrenderstate)
        {
            t.add(confirmbtn.getButton());
            t.row();
            t.add(cancelbtn.getButton());
            components.add(confirmbtn,cancelbtn);
        }
        else
        {
            t.add(rockbtn.getButton(),paperbtn.getButton(),scissorbtn.getButton());
            t.row();
            t.add(surrenderbtn.getButton()).colspan(3);
            components.add(rockbtn,paperbtn,scissorbtn,surrenderbtn);
        }

        container= new Container<Table>(t);
        container.top();
        actor=container;
    }

    @Override
    public void scale(float ratio) {
        table.getCell(actor).size((int) Math.ceil(size.x*ratio),(int)Math.ceil(size.y*ratio));
        table.getCell(actor).padTop(padding[0]*ratio);
        table.getCell(actor).padRight(padding[1]*ratio);
        table.getCell(actor).padBottom(padding[2]*ratio);
        table.getCell(actor).padLeft(padding[3]*ratio);
        for (UIComponent component:components)component.scale(ratio);
    }

    public boolean isSurrenderstate() {
        return surrenderstate;
    }

    public Container<Table> getContainer() {
        return container;
    }

    public UIButton getSurrenderbtn() {
        return surrenderbtn;
    }

    public UIButton getConfirmbtn() {
        return confirmbtn;
    }

    public UIButton getCancelbtn() {
        return cancelbtn;
    }

    public void hideattackbtns(){
        rockbtn.getButton().setVisible(false);
        paperbtn.getButton().setVisible(false);
        scissorbtn.getButton().setVisible(false);
    }

    public void showattackbtns(){
        rockbtn.getButton().setVisible(true);
        paperbtn.getButton().setVisible(true);
        scissorbtn.getButton().setVisible(true);
    }

    public void clearselection(){
        selected=null;
        rockbtn.setbackground("rockicon");
        paperbtn.setbackground("papericon");
        scissorbtn.setbackground("scissorsicon");
    }

    public void setselectedButton(UIButton selected){
        this.selected=selected;
    }

    public UIButton getselectedButton(){
        return  selected;
    }


}
