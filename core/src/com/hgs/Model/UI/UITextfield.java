package com.hgs.Model.UI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.hgs.Model.PixelBrawl;

public class UITextfield extends UIComponent {

    TextField textfield;
    TextField.TextFieldStyle style;
    Container<Label> labelcontainer;
    boolean passwordmode;
    public UITextfield(Table table, String name, Vector2 size,boolean passwordmode) {
        super(table, name, size);
        this.passwordmode=passwordmode;
        setStyle();
    }

    public UITextfield(Table table, String name, Vector2 size,boolean passwordmode, int[] padding) {
        super(table, name, size, padding);
        this.passwordmode=passwordmode;
        setStyle();
    }

    @Override
    void setStyle() {
          style= new TextField.TextFieldStyle();
          style.font=PixelBrawl.font1;
          style.fontColor=Color.WHITE;
          style.cursor= PixelBrawl.skin.getDrawable("cursor");
          style.background= PixelBrawl.skin.getDrawable("textfield");
          style.selection= PixelBrawl.skin.getDrawable("selected");
          textfield=new TextField("",style);
          textfield.setPasswordMode(passwordmode);
          textfield.setMaxLength(16);
          textfield.setAlignment(Align.center);
          textfield.setPasswordCharacter('*');
          style.font.getData().setScale(1f);
          actor=textfield;
    }

    @Override
    public void scale(float ratio) {
        table.getCell(actor).size((int) Math.ceil(size.x),(int)Math.ceil(size.y));
        table.getCell(actor).padTop(padding[0]*ratio);
        table.getCell(actor).padRight(padding[1]*ratio);
        table.getCell(actor).padBottom(padding[2]*ratio);
        table.getCell(actor).padLeft(padding[3]*ratio);

    }

    public TextField getTextfield() {
        return textfield;
    }
    public void enableFilter(){
        textfield.setTextFieldFilter(new TextField.TextFieldFilter() {
            @Override
            public boolean acceptChar(TextField textField, char c) {
                if (Character.toString(c).matches("^[a-zA-Z0-9]")) {
                    return true;
                }
                return false;
            }
        });
    }
}
