package com.hgs.Model.UI;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.hgs.Model.Game.Upgradable;
import com.hgs.Model.PixelBrawl;
import com.hgs.Model.Scenes.ShopScene;


public class UIShop extends UIComponent {
    ShopScene scene;
    Container<Table>container;
    Table t;
    Array<UIComponent> components;
    Image background;
    UILabel titlelabel;
    UIImage typeslot;
    UIImage skillslot;
    UILabel levellabel;
    UILabel effectlabel;
    UILabel costlabel;
    UILabel chancelabel;
    UILabel successlabel;
    UIButton upgradebutton;
    Upgradable upgradable;
    public UIShop(ShopScene scene,Upgradable upgradable,Table table, String name, Vector2 size) {
        super(table, name, size);
        this.upgradable=upgradable;
        this.scene=scene;
        setStyle();
    }

    public UIShop(ShopScene scene,Upgradable upgradable,Table table, String name, Vector2 size, int[] padding) {
        super(table, name, size, padding);
        this.upgradable=upgradable;
        this.scene=scene;
        setStyle();
    }

    @Override
    void setStyle() {
        t=new Table();
        t.top();
        container= new Container<Table>(t);
        container.top();
        background= new Image(PixelBrawl.skin.getDrawable("infopane"));
        components= new Array<UIComponent>();
        container.setBackground(background.getDrawable());
        actor=container;

        titlelabel= new UILabel(upgradable.getType().name(), Align.top,t,"titlelabel",new Vector2(size.x,size.y/10),1.1f,new int[]{(int) (size.y/40),0,0,0});
        if (upgradable.getType().equals(Upgradable.Type.Rock)||upgradable.getType().equals(Upgradable.Type.Paper)||upgradable.getType().equals(Upgradable.Type.Scissors)){
            skillslot= new UIImage(t,upgradable.getSkillicon(),"slot",new Vector2(size.x/2.7f,size.y/5.3f),new int[]{20,0,0,0});
            typeslot= new UIImage(t,upgradable.getTypeicon(),"slot",new Vector2(size.x/2.7f,size.y/5.3f),new int[]{20,0,0,0});
        }else {
            typeslot= new UIImage(t,upgradable.getTypeicon(),"slot",new Vector2(size.x/2.7f,size.y/5.3f),new int[]{20,0,0,0});
        }
        levellabel = new UILabel("Level: "+upgradable.getLevel(), Align.center,t,"tierlabel",new Vector2(size.x,size.y/16),0.8f,new int[]{(int) (size.y/40),0,0,0});
        effectlabel = new UILabel(upgradable.getEffect(), Align.center,t,"damagelabel",new Vector2(size.x,size.y/16),0.6f,new int[]{(int) (size.y/40),0,0,0});
        costlabel= new UILabel("Cost: "+upgradable.getCost()+"$", Align.center,t,"costlabel",new Vector2(size.x,size.y/16),0.8f,new int[]{(int) (size.y/40),0,0,0});
        chancelabel= new UILabel("Upgrade Chance "+upgradable.getChance()+"%", Align.center,t,"chancelabel",new Vector2(size.x,size.y/16),0.5f,new int[]{(int) (size.y/40),0,0,0});
        successlabel= new UILabel("", Align.center,t,"successlabel",new Vector2(size.x,size.y/16),0.8f,new int[]{(int) (size.y/20),0,0,0});
        successlabel.getLabel().setColor(0f,1f,0.5f,1f);
        upgradebutton = new UIButton("Upgrade",t,"upgradebtn",new Vector2(size.x/1.14f,size.y/8),new int[]{(int) (size.y/40),0,0,0});
        if (upgradable.getType().equals(Upgradable.Type.Rock)||upgradable.getType().equals(Upgradable.Type.Paper)||upgradable.getType().equals(Upgradable.Type.Scissors)){
            t.add(titlelabel.getlabelcontainer()).colspan(2);
            t.row();
            t.add(typeslot.getImage());
            t.add(skillslot.getImage());
            t.row();
            t.add(levellabel.getlabelcontainer()).colspan(2);
            t.row();
            t.add(effectlabel.getlabelcontainer()).colspan(2);
            t.row();
            t.add(costlabel.getlabelcontainer()).colspan(2);
            t.row();
            t.add(chancelabel.getlabelcontainer()).colspan(2);
            t.row();
            t.add(successlabel.getlabelcontainer()).colspan(2);
            t.row();
            t.add(upgradebutton.getButton()).colspan(2);
        }else {
            t.add(titlelabel.getlabelcontainer());
            t.row();
            t.add(typeslot.getImage());
            t.row();
            t.add(levellabel.getlabelcontainer());
            t.row();
            t.add(effectlabel.getlabelcontainer());
            t.row();
            t.add(costlabel.getlabelcontainer());
            t.row();
            t.add(chancelabel.getlabelcontainer());
            t.row();
            t.add(successlabel.getlabelcontainer());
            t.row();
            t.add(upgradebutton.getButton());
        }
        components.add(titlelabel,typeslot, levellabel);
        if (upgradable.getType().equals(Upgradable.Type.Rock)||upgradable.getType().equals(Upgradable.Type.Paper)||upgradable.getType().equals(Upgradable.Type.Scissors)){
          components.add(skillslot);
        }
        components.add(effectlabel,costlabel,chancelabel,successlabel);
        components.add(upgradebutton);
        upgradebutton.getButton().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                int toupgrade=0;
                switch (upgradable.getType()){
                    case Rock:
                        toupgrade=0;
                        break;
                    case Paper:
                        toupgrade=1;
                        break;
                    case Scissors:
                        toupgrade=2;
                        break;
                    case Armor:
                        toupgrade=3;
                        break;
                    case Health:
                        toupgrade=4;
                        break;
                    case MagicResist:
                        toupgrade=5;
                        break;
                    default:
                        break;
                }
                scene.upgrade(toupgrade);
            }
        });

    }

    @Override
    public void scale(float ratio) {
        container.setSize((int)Math.ceil(size.x*ratio),(int)Math.ceil(size.y*ratio));
        table.getCell(actor).size((int) Math.ceil(size.x*ratio),(int)Math.ceil(size.y*ratio));
        table.getCell(actor).padTop(padding[0]*ratio);
        table.getCell(actor).padRight(padding[1]*ratio);
        table.getCell(actor).padBottom(padding[2]*ratio);
        table.getCell(actor).padLeft(padding[3]*ratio);
        for (UIComponent component:components)component.scale(ratio);
    }

    public void update(){
        typeslot.getImage().setDrawable(upgradable.getTypeicon());
        if (upgradable.getType().equals(Upgradable.Type.Rock)||upgradable.getType().equals(Upgradable.Type.Paper)||upgradable.getType().equals(Upgradable.Type.Scissors)){
            skillslot.getImage().setDrawable(upgradable.getSkillicon());
        }
        levellabel.getLabel().setText("Level: "+upgradable.getLevel());
        effectlabel.getLabel().setText(upgradable.getEffect());
        costlabel.getLabel().setText("Cost: "+upgradable.getCost()+"$");
        chancelabel.getLabel().setText("Upgrade Chance "+upgradable.getChance()+"%");

        if (upgradable.getLevel()==upgradable.getMaxlevel()){
            costlabel.getlabelcontainer().setVisible(false);
            chancelabel.getlabelcontainer().setVisible(false);
            upgradebutton.getButton().setVisible(false);
        }
    }

    public Upgradable getUpgradable() {
        return upgradable;
    }

    public UILabel getSuccesslabel() {
        return successlabel;
    }

    public Container<Table> getContainer() {
        return container;
    }

}
